//
//  poc3App.swift
//  poc3
//
//  Created by Nayab Mehar on 06/05/24.
//

import SwiftUI
@_exported import Core

@main
struct poc3App: App {
  
    var body: some Scene {
      
      let entityVM = EntityViewModel()
      
    WindowGroup {
            ContentView()
    }.environmentObject(entityVM)
        
      WindowGroup(id: "1"){
          VideoWindow()
        }
      
        ImmersiveSpace(id: "ImmersiveSpace") {
            ImmersiveView()
            .environmentObject(entityVM)
        }.immersionStyle(selection: .constant(.full), in: .full)
    }
}

