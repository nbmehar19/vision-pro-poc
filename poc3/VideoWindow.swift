//
//  VideoWindow.swift
//  poc3
//
//  Created by Nayab Mehar on 06/05/24.
//

import SwiftUI
import AVKit
import RealityKit

struct VideoWindow : View {
  
  @State var player = AVPlayer()
  var videoUrl: String = "video.mp4"
//  let material = VideoMaterial(avPlayer: player)
  let avPlayer = AVPlayer(url:  Bundle.main.url(forResource: "video", withExtension: "mp4")!)
  var body: some View {
    VideoPlayer(player: player)
            .onAppear() {
//                player = AVPlayer(url:  Bundle.main.url(forResource: "video", withExtension: "mp4")!)
                  playVideoMaterial(player1: player)
            }
  }
  
  func playVideoMaterial(player1 : AVPlayer) {
//    if let url = Bundle.main.url(forResource: "video", withExtension: "mp4") {


        // Create an AVPlayer instance to control playback of that movie.
//        let player = AVPlayer(url: url)


        // Instantiate and configure the video material.
        let material = VideoMaterial(avPlayer: player1)


        // Configure audio playback mode.
//        material.controller.audioInputMode = .spatial


        // Create a new model entity using the video material.
      let modelEntity = ModelEntity(mesh: CubeMesh.cube, materials: [material])


        // Start playing the video.
        player1.play()
//    }
  }
  
  
}


struct CubeMesh {
  static var cube: MeshResource {
    return MeshResource.generateBox(size: 200) // Adjust size as needed
  }
}
