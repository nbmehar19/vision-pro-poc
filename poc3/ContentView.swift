//
//  ContentView.swift
//  poc3
//
//  Created by Nayab Mehar on 06/05/24.
//

import SwiftUI
import RealityKit
import RealityKitContent

struct ContentView: View {

    @State private var showImmersiveSpace = false
    @State private var immersiveSpaceIsShown = false
    @EnvironmentObject var entityVM : EntityViewModel
//    @State var isZoomedIn = false
    @Environment(\.openImmersiveSpace) var openImmersiveSpace
    @Environment(\.dismissImmersiveSpace) var dismissImmersiveSpace
    @Environment(\.openWindow) var openWindow

    var body: some View {
        VStack {
            Model3D(named: "Scene", bundle: realityKitContentBundle)
                .padding(.bottom, 50)

//            Text("Hello, world!")
            
//          CachedAsyncImage(
//                    url: URL(
//                      string:
//                        "https://s3-alpha-sig.figma.com/img/a615/0e03/568f96ab738137b33a68a1c8b9ec0990?Expires=1715558400&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=IjLtm7G~5qBBQVP9zx0SxJU5wTaUqwBPfNp0-2Qj59Y-1JpPPBUZXhtcqLr1j6dp2BUKCjPtxCl5VnJoolFo-hXSQCk330LHXlxI51wp4BwUJQLkuVb-OLwd~dzn1z9gqhBYcMIyQL-ntFezoQwcLxRQ3jzw2~h0FehFYb3fDJ4XcOWeS3r9lPAe7k6E7XeK5f4ul5oQDeK5y9bWapIyPe9CT04Jb06pJYOWMkxv-gmWJqjbbbcIi-ImTM7yR5cBGNqYS2KN2ltEeLmPXsn8xI~NWOb-Vfw0IJ8rfEevqNiD2NhfiZ3Y1IU3w7F2loi0KibhJMu7rM2hrUZBPBbqfw__"
//                    )
//                  ) {
//                    image in
//                    image
//                      .resizable()
//                      .frame(
//                        width: 172,
//                        height: 208
//                      )
//                      .clipped()
//                  } placeholder: {
//                    Color.black.border(.green)
//                  }

            Toggle("Show ImmersiveSpace", isOn: $showImmersiveSpace)
                .font(.title)
                .frame(width: 360)
                .padding(24)
                .glassBackgroundEffect()
          
//          Button("Play Animation"){
//            entityVM.playAnimation.toggle()
//          }
            
          Toggle("Play Animation", isOn: $entityVM.playAnimation)
              .font(.title)
              .frame(width: 360)
              .padding(24)
              .glassBackgroundEffect()
          
          Toggle("Zoom", isOn: $entityVM.isZoomedIn)
              .font(.title)
              .frame(width: 360)
              .padding(24)
              .glassBackgroundEffect()
          
          Toggle("Change Angle", isOn: $entityVM.changeAngle)
              .font(.title)
              .frame(width: 360)
              .padding(24)
              .glassBackgroundEffect()
          
        }
        .padding()
        .onChange(of: showImmersiveSpace) { _, newValue in
            Task {
                if newValue {
                  
                    switch await openImmersiveSpace(id: "ImmersiveSpace") {
                    case .opened:
                        immersiveSpaceIsShown = true
                        openWindow(id: "1")
                    case .error, .userCancelled:
                        fallthrough
                    @unknown default:
                        immersiveSpaceIsShown = false
                        showImmersiveSpace = false
                    }
                } else if immersiveSpaceIsShown {
                    await dismissImmersiveSpace()
                    immersiveSpaceIsShown = false
                }
            }
        }
    }
}

#Preview(windowStyle: .automatic) {
    ContentView()
}
