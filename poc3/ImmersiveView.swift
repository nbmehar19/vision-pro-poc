//
//  ImmersiveView.swift
//  poc3
//
//  Created by Nayab Mehar on 06/05/24.
//

import SwiftUI
import RealityKit
import RealityKitContent

struct ImmersiveView: View {
  @EnvironmentObject var entityVM : EntityViewModel
  @State var entity = Entity()
  
  
    var body: some View {
        RealityView { content in
            // Add the initial RealityKit content
            if let entity = try? await Entity(named: "Immersive", in: realityKitContentBundle) {
              self.entity = entity
//              let animation = entity.availableAnimations[0]
//              entity.playAnimation(animation.repeat())
              
              content.add(entity)
              print(entity.transform.translation)
              
//              transform.translation =  transform.translation + SIMD3(730000, 73000, 7300)
//              entity.move(to: transform, relativeTo: nil)

                // Add an ImageBasedLight for the immersive content
                guard let resource = try? await EnvironmentResource(named: "ImageBasedLight") else { return }
              let iblComponent = ImageBasedLightComponent(source: .single(resource), intensityExponent: 0.5)
              entity.components.set(iblComponent)
              entity.components.set(ImageBasedLightReceiverComponent(imageBasedLight: entity))

                // Put skybox here.  See example in World project available at
                // https://developer.apple.com/
            }
        }
        .onChange(of: entityVM.playAnimation){
          newValue in
          if(newValue){
            let animation = entity.availableAnimations[0]
            entity.playAnimation(animation.repeat(count:2))
            entityVM.playAnimation = false
          }
        }
        .onChange(of: entityVM.isZoomedIn){newValue in
          if(newValue){
            var transform = entity.transform
            print(transform.translation)
            transform.rotation *= simd_quatf(angle: .pi/2, axis: [-1,0,0])
            transform.translation = transform.translation + SIMD3(0,370,-50)
             entity.move(to: transform, relativeTo: nil, duration: 2)
            
          }else{
            var transform = entity.transform
            transform.translation = transform.translation + SIMD3(0,-370,50)
            transform.rotation *= simd_quatf(angle: .pi/2, axis: [1,0,0])
            entity.move(to: transform, relativeTo: nil, duration: 2)
            
          }
                      
        }
    }
}

//#Preview(immersionStyle: .full) {
//    ImmersiveView()
//}
