//
//  EntityViewModel.swift
//  poc3
//
//  Created by Nayab Mehar on 09/05/24.
//

import Foundation


class EntityViewModel: ObservableObject {
    @Published var isZoomedIn: Bool = false
    @Published var playAnimation: Bool = false
  @Published var changeAngle: Bool = false
}
